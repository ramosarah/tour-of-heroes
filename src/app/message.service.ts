import { Injectable } from '@angular/core';
//ici on injecte des infos mais on se fou de savoir de ou elle viennent. Bisous
@Injectable({
  providedIn: 'root',
})
export class MessageService {
  messages: string[] = [];

  add(message: string) {
    this.messages.push(message);
  }

  clear() {
    this.messages = [];
  }
}
