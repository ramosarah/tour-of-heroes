import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Dr NiceM' },
  { id: 12, name: 'NarcoM' },
  { id: 13, name: 'BombastoM' },
  { id: 14, name: 'CeleritasM' },
  { id: 15, name: 'MagnetaM' },
  { id: 16, name: 'RubberManM' },
  { id: 17, name: 'DynamaM' },
  { id: 18, name: 'Dr IQM' },
  { id: 19, name: 'MagmaM' },
  { id: 20, name: 'TornadoM' }
];
